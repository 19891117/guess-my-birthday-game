from random import randint
#ask your name
your_name = input("Hi! What is your name? ")
#guess your birth month and year 5 times
# Guess «guess number» : «name» were you born in «m» / «yyyy» ?
# then prompts with "yes or no?"

for i in range(5):
    your_birthmonth = randint(1,12)
    your_birthyear = randint(1924,2004)

    print ("Guess", i+1, your_name, "Were you born in", your_birthmonth,"/",your_birthyear,"?")
    answer = input("yes/no? (case matters)")

    if answer == "yes":
        print ("I knew it!")
        break

    elif i == 4:
        print("You used all your tries!")

    else:
        print("Drat! Lemme try again!")
